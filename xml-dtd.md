## Valide XML-Dokumente gegen eine Document-Type-Definition erstellen

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111970765687640258</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/xml-dtd</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Alle XML-Dokumente müssen wohlgeformt sein, also den allgemeinen XML-Regeln genügen. Mit Hilfe der DocumentTypeDefinition oder mit XML-Schema können aber darüber hinaus spezifische Regeln an die Abfolge von Elementen und Attributen gestellt werden. Ein XML-Dokument, das solchen Regeln folgt, wird als valide bezeichnet. Dieser Artikel beschreibt die Grundlagen zur Erstellung einer Document-Type-Definition._

Dieser Artikel ist Bestandteil einer Serie zu den _XML_-Grundlagen:

- Teil 1: [XML-Grundlagen und Wohlgeformtheit: Hierarchische Daten mit XML speichern](https://oer-informatik.de/xml-einstieg) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 2: [XML-Dokumente gegen eine Document-Type-Definition validieren](https://oer-informatik.de/xml-dtd) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 3: [Namespaces für komplexere XML-Dokumente definieren](https://oer-informatik.de/xml-namespaces) 

- Teil 4: [Die Struktur von XML-Dokumente gegen ein XSD-Schema validieren](https://oer-informatik.de/xml-schema-struktur)

- Teil 5: [Datentypen-Definitionen eines XML-Schema zur Validierung nutzen](https://oer-informatik.de/xml-schema-datentypen)

<span class="kann-liste">

Nach Durchlesen des Artikels kann ich...

- ... eine Document-Type-Definition _inline_ oder extern einbinden,

- ... Elementefolgen, Optionalitäten und Häufigkeit mit DTD definieren,

- ... festlegen, ob ein Element über Inhalte verfügen muss, und ob dies Unterelemente oder Text ist,

- ... definieren, ob ein Element über Attribute verfügt und welchen Typs die Inhalte sind.

</span>

### Festlegen, in welcher Reihenfolge und Anzahl XML-Elemente aufeinander folgen

Mithilfe einer _Document-Type-Definition_ (DTD) können Regeln an den Aufbau und die Abfolge von Elementen und Attributen getroffen werden. _DTDs_  lassen sich intern in _XML_-Dokumente integrieren (siehe folgendes Beispiel) oder in einer gesonderten Datei verwalten (Beispiele unten). 

Wir betrachten den Aufbau einer _DTD_ für eine _XML-Datei an folgendem Beispiel (ab Zeile 2 beginnt die DTD, mit `<adressen>` beginnt das eigentliche XML-Rootelement): 

```xml
<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE adressen [
  <!ELEMENT adressen (adresse)+ >
    <!ELEMENT adresse (vorname, nachname, telefon?, kommentar?, id?)> 
      <!ELEMENT vorname (#PCDATA)>
      <!ELEMENT nachname (#PCDATA)>
      <!ELEMENT telefon (mobil|festnetz)*>
        <!ELEMENT mobil (#PCDATA)>
        <!ELEMENT festnetz (#PCDATA)>
      <!ELEMENT id EMPTY>
        <!ATTLIST id
          personal_id CDATA #REQUIRED
          mitarbeiter_id CDATA #IMPLIED
          exists CDATA #FIXED "true" >
      <!ELEMENT kommentar ANY>
]>

<adressen>
    <adresse>
        <vorname>Max</vorname>
        <nachname>Mustermann</nachname>
        <telefon>
          <mobil>01234/567890</mobil>
        </telefon>
        <kommentar>
          Wohnt bei <vorname>Peter</vorname>
        </kommentar>
        <id personal_id="12345" mitarbeiter_id="6789"/>
    </adresse>
</adressen>
```

Die interne _DTD_ wird umschlossen von `<!DOCTYPE ... [` und `]>`, wobei anstelle der Punkte der Name des Root-Elements steht.

Darauf folgt die Beschreibung der Elemente mit Elementeinhalt nach dem Muster:

```xml
<!ELEMENT name (kindelement1, kindelement1)>
```

Die Kindelemente können dabei in Klammern `(...)` als Gruppen zusammengefasst und mit Konnektoren als Alternative (`|`) oder als Sequenz (`,`) formuliert werden:

|Beispiel|Konnektor|Beschreibung|
|:---|---|:---|
|`<!ELEMENT adresse (vorname, nachname)>`|`,`|Liste (wird gefolgt von)|
|`<!ELEMENT telefon (mobil|festnetz)>`|`|`|Alternative (entweder / oder)|

Für jede Gruppe und für jedes Kindelement kann eine Quantität angegeben werden:

|Beispiel|Quantität|Beschreibung|
|:---|---|:---|
|`<!ELEMENT adresse (telefon?)>`|`?`|`telefon` ist in `adresse` optional (kein- oder einmal)|
|`<!ELEMENT adressen (adresse)+ >`|`+`|`adresse` kommt in `adressen` ein oder mehrmals vor|
|`<!ELEMENT telefon (mobil|festnetz)*>`|`*`|die Gruppe kommt in `telefon` einmal oder mehrmals vor|
|`<!ELEMENT adresse (vorname, nachname)>`|_nichts_|`vorname` und `nachname` müssen in `adresse` genau einmal vorkommen|

Neben Kindelementen kann für ein Element auch noch festgelegt werden, dass es leer ist (`EMPTY`), beliebigen Inhalt haben kann (`ANY`) oder parsbaren Text enthält (`PCDATA`):

|Beispiel|Quantität|Beschreibung|
|:---|---|:---|
|`<!ELEMENT vorname #PCDATA>`|`PCDATA`|`vorname` darf nur parsbaren Text enthalten (**p**arsed **c**haracter **data**)|
|`<!ELEMENT kommentar ANY>`|`ANY`|`kommentar` darf beliebige Kindelemente oder Daten enthalten|
|`<!ELEMENT id EMPTY>`|`EMPTY`|`id` darf keinen Elementeinhalt enthalten, ist also ein leeres Element (lediglich Attribute, siehe unten)|

Die obigen Beispiele können natürlich auch kombiniert werden, man spricht dann von _Mixed Contend_:

|Beispiel|Beschreibung|
|:---|:---|
|`<!ELEMENT vorname (#PCDATA | künstlername | ordensname)*>`| **wichtig**: `#PCDATA` muss als erstes genannt sein, die Reihenfolge der nachfolgenden Elemente kann nicht festgelegt werden und am Ende muss der Asterisk stehen|

Auch für Attribute können unterschiedliche Festlegungen getroffen werden, beispielsweise können besondere Typen angegeben werden:

|Beispiel|Typ|Beschreibung|
|:---|---|:---|
|`<!ATTLIST id personal_id CDATA>`|`CDATA`|`personal_id` enthält Text inkl. aller Whitespaces - das ist der Standardfall<br/>**wichtig**: bei Attributen wird `CDATA` verwendet, bei Elementinhalten `PCDATA`|
|`<!ATTLIST id personal_id ID>`|`ID`|Der Wert eines `ID`-Attributs muss das Element eindeutig identifizieren und einzigartig sein. Außerdem muss der Wert den Namensregeln für Elemente entsprechen (also "1" ist unzulässig). Es darf nur ein `ID` Attribut je Element gebe. |
|`<!ATTLIST id aktiv (ja | nein)>`|_ENUM_<br/>Aufzählung|Der Wert eines Aufzählungs-Attributs einem der genannten Werte entsprechen (hier: `ja` oder `nein`).|
|`<!ATTLIST id personal_id 	NMTOKEN>`|`NMTOKEN`|Bei Attributen vom Typ `NMTOKEN` werden im Gegensatz zu `CDATA` beim Parsen beispielsweise Whitespaces entfernt. Details siehe [Nr. 3.3.3 im Standard](https://www.w3.org/TR/REC-xml/#AVNormalize)|

Es gibt eine Reihe weiterer Attributtypen, die sich [in der Spezifikation nachlesen lassen](https://www.w3.org/TR/xml/#sec-attribute-types). Darüber hinaus können auch Optionalitäten und Defaultwerte angegeben werden. Die Defaultwerte folgend der Optionalität in Anführungszeichen (siehe Tabelle bei `#FIXED`):


|Beispiel|Optionalität|Beschreibung|
|:---|---|:---|
|`<!ATTLIST id personal_id CDATA #REQUIRED>`|`#REQUIRED`|Ein Attribut `personal_id` ist erforderlich|
|`<!ATTLIST id mitarbeiter_id CDATA #IMPLIED>`|`#IMPLIED`|das Attribut ist optional|
|`<!ATTLIST id exists CDATA #FIXED "true">`|`#FIXED defaultwert`|Der Attributwert darf hier nur der Defaultwert sein (Beispiel: `id` muss "true" sein). Bei einem anderen Wert validiert das Dokument nicht, bei einem fehlenden Wert wird beim Parsen `id="true"` ergänzt|

Die wesentlichen Bestandteile einer DTD für unser Beispiel habe ich hier nochmal annotiert:

![Die DTD für adressen - wie oben als Text, diesmal jedoch mit der Kurzzusammenfassung der Bestandteile](images/xml-dtd-beispiel.png)

### Externe DTD

Da die Document Type Definition häufig einen Standard festlegt, über den Systeme miteinander XML-Dokumente austauschen, wird sie häufig nicht innerhalb des XML-Dokuments übermittelt, sondern als Link veröffentlicht (oder als lokale Datei), die dann im Dokument referenziert wird: 

```xml
<!DOCTYPE adressen SYSTEM "adressen.dtd">
```

### Webservices zur Validierung

Natürlich überprüft niemand von Hand, ob eine _XML_-Datei gegen eine _DTD_ valide ist. Dafür gibt es Parser für alle Programmiersprachen. Ein paar Beispiele für Webservices, die das übernehmen finden sich hier:

- [xmlvalidarion.com](https://www.xmlvalidation.com/)


### Links und weitere Informationen

- [Der XML-Standard 1.0](https://www.w3.org/TR/xml/)

- [Der seltener genutzte XML-Standard 1.1](https://www.w3.org/TR/xml11/)

- Infos von [w3schools](https://www.w3schools.com/xml/default.asp) zu XML
