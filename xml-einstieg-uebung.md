## Extensible Markup Language (XML) - Übungsaufgaben zu wohlgeformten Dokumenten

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/111970777730799923</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/xml-einstieg-uebung</span>

> **tl/dr;** _(ca. 8 min Bearbeitungszeit): Es soll beurteilt werden, ob XML-Dokumenten wohlgeformt sind. Übungsaufgaben zum [Einstiegs-Artikel zu XML](https://oer-informatik.de/xml-einstieg)._

Dieser Artikel ist Bestandteil einer Serie zu den _XML_-Grundlagen:

- Teil 1: [XML-Grundlagen und Wohlgeformtheit: Hierarchische Daten mit XML speichern](https://oer-informatik.de/xml-einstieg) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 2: [XML-Dokumente gegen eine Document-Type-Definition validieren](https://oer-informatik.de/xml-dtd) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 3: [Namespaces für komplexere XML-Dokumente definieren](https://oer-informatik.de/xml-namespaces) 

- Teil 4: [Die Struktur von XML-Dokumente gegen ein XSD-Schema validieren](https://oer-informatik.de/xml-schema-struktur)

- Teil 5: [Datentypen-Definitionen eines XML-Schema zur Validierung nutzen](https://oer-informatik.de/xml-schema-datentypen)

### Wohlgeformt oder nicht?

|XML-Dokument|Wohlgeformt? Mit Begründung!|
|:---|:---|
|`<woherSollIchDasWissen/>`<br/><button onclick="toggleAnswer('xml1')">show</button>|<span class="hidden-answer" id="xml1">Wohlgeformt. Ein leeres Wurzelelement, keine Verstöße.</span>|
|`<!-- Was ist das denn für eine Frage --><langweilig/>`<br/><button onclick="toggleAnswer('xml2')">show</button>|<span class="hidden-answer" id="xml2">Wohlgeformt. Ein gültiger Kommentar und ein leeres Wurzelelement, keine Verstöße.</span>|
|`<text>Und wann kommen die schweren Fragen?</text/>`<br/><button onclick="toggleAnswer('xml3')">show</button>|<span class="hidden-answer" id="xml3">Nicht wohlgeformt. Endtag fehlerhaft</span>|
|`< text>Was ist denn das für ein XML Dokument?</ text>`<br/><button onclick="toggleAnswer('xml4')">show</button>|<span class="hidden-answer" id="xml4">Nicht wohlgeformt. Elementnamen dürfen nicht mit Leerzeichen beginnen.</span>|	
|`<?xml version=1.0 encoding=UTF-8 standalone=yes?>`<br/>`<!-- kommt jetzt noch etwas? oder nicht?-->`<br/>`<inhalt>noe</inhalt>`<br/><button onclick="toggleAnswer('xml5')">show</button>|<span class="hidden-answer" id="xml5">Nicht wohlgeformt. Die Attribute der XML-Deklaration müssen in Anführungszeichen gefasst werden.</span>|
|`<Text>Und das hier erst!</text><Text>Und das hier erst!</text>`<br/><button onclick="toggleAnswer('xml6')">show</button>|<span class="hidden-answer" id="xml6">Nicht wohlgeformt. Elementnamen mussen _case sensitive_ geschlossen werden.</span>|
|`<Über>Mit XML muss man sich eingehend beschäftigen </Über>`<br/><button onclick="toggleAnswer('xml7')">show</button>|<span class="hidden-answer" id="xml7">Wohlgeformt. Umlaute sind auch als Startzeichen erlaubt - lediglich ein kombiniertes Zeichen (also erstes Zeichen: die beiden Punkte / zweites Zeichen: das U) wäre es nicht.</span>|
|`<!-- minimal -->`<br/>`<beispiel 1text="hallo" 2text="welt"/>`<br/><button onclick="toggleAnswer('xml15a')">show</button>|<span class="hidden-answer" id="xml15a">Nicht wohlgeformt. Element- und Attributnamen dürfen nicht mit Zahlen, Punkt oder Bindestrich anfangen.</span>|
|`<Über den Autor>Douglas Adams hätte XML erfinden können</Über den Autor >`<br/><button onclick="toggleAnswer('xml8')">show</button>|<span class="hidden-answer" id="xml8">Nicht wohlgeformt. Der Name selbst darf keine Leerzeichen enthalten. "den" wird so als Attribut interpretiert, dem fehlt aber wiederum der Attributwert.</span>|
|`<xmlTextInhalt>Und warum soll ich das lesen?</xmlTextInhalt >`<br/><button onclick="toggleAnswer('xml9')">show</button>|<span class="hidden-answer" id="xml9">Wohlgeformt. Whitespaces innerhalb eines Tags sind zulässig.</span>|
|`<born date="18.05.1970" />`<br/><button onclick="toggleAnswer('xml10')">show</button>|<span class="hidden-answer" id="xml10">Wohlgeformt. Leeres Wurzelelement mit Attributen ist zulässig.</span>|
|`<?xml version="1.0" encoding="UTF-16" standalone="yes" ?>`<br/>`<!--- minimal --->`<br/>`<text />`<br/><button onclick="toggleAnswer('xml16')">show</button>|<span class="hidden-answer" id="xml16">Nicht wohlgeformt. Am Beginn und am Ende des Kommentars darf hinter dem `<!--` und vor dem `-->` kein weiterer Bindstrich stehen.</span>|
|`<bornAgain date='18.05.1970' />`<br/><button onclick="toggleAnswer('xml11')">show</button>|<span class="hidden-answer" id="xml11">Wohlgeformt. Leeres Wurzelelement mit Attributen ist zulässig.</span>|
|`<born place="Bielefeld am Teutoburger Wald' />`<br/><button onclick="toggleAnswer('xml12')">show</button>|<span class="hidden-answer" id="xml12">Nicht wohlgeformt. Doppeltes Anführungszeichen öffnet den Attributwert, ein einfaches schließt es.</span>|
|`<?xml version="1.0" encoding="ISO-8859-1" standalone="yes" ?>`<br/>`<!-- Kurzes XML -- Mit nur einem Element -->`<br/>`<beispiel>Hier ist Beispieltext</beispiel>`<br/><button onclick="toggleAnswer('xml13')">show</button>|<span class="hidden-answer" id="xml13">Nicht wohlgeformt. Zwei Bindestriche hintereinander sind innerhalb eines Kommentars unzulässig.</span>|
|`<?xml version="1.0" encoding="ISO-8859-1" standalone="yes" ?>`<br/>`<!-- Kurzes XML -- Mit nur einem Element -->`<br/>`<bEisPieL>Hier ist Beispieltext</bEisPieL>`<br/><button onclick="toggleAnswer('xml14')">show</button>|<span class="hidden-answer" id="xml14">Wohlgeformt. Text darf Camel-Case haben.</span>|
|`<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>`<br/>`<!-- minimal -->`<br/>`<xmlbeispiel />`<br/><button onclick="toggleAnswer('xml15')">show</button>|<span class="hidden-answer" id="xml15">Nicht wohlgeformt. Elementnamen dürfen nicht mit `xml` anfangen (groß- kleingeschrieben oder kombiniert).</span>|
|`<!-- minimal -->`<br/>`<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>`<br/>`<beispiel />`<br/><button onclick="toggleAnswer('xml17')">show</button>|<span class="hidden-answer" id="xml17">Nicht wohlgeformt. Vor der XML-Deklaration darf nichts stehen.</span>|		

	