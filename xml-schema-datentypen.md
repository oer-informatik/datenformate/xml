## Datentypen-Definitionen eines XML-Schema zur Validierung nutzen

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111970765687640258</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/xml-schema-datentypen</span>

> **tl/dr;** _(ca. 10 min Lesezeit): XML Schema (XSD) stellen einen gewaltigen Sprachumfang zur Deklaration und Spezifizierung von Datentypen bereit. Neben vielen bereits vorhanden Datentypen und den komplexen eigenen Datentypen bietet sich die Möglichkeit, über zahlreiche Restrictions die Werte individuell einzugrenzen, und somit die Validität über die reine Struktur auf den Inhalt der Daten auszuweiten._

Dieser Artikel ist Bestandteil einer Serie zu den _XML_-Grundlagen:

- Teil 1: [XML-Grundlagen und Wohlgeformtheit: Hierarchische Daten mit XML speichern](https://oer-informatik.de/xml-einstieg) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 2: [XML-Dokumente gegen eine Document-Type-Definition validieren](https://oer-informatik.de/xml-dtd) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 3: [Namespaces für komplexere XML-Dokumente definieren](https://oer-informatik.de/xml-namespaces) 

- Teil 4: [Die Struktur von XML-Dokumente gegen ein XSD-Schema validieren](https://oer-informatik.de/xml-schema-struktur)

- Teil 5: [Datentypen-Definitionen eines XML-Schema zur Validierung nutzen](https://oer-informatik.de/xml-schema-datentypen)

<span class="kann-liste">

Nach Durchlesen des Artikels kann ich...

- ... passende einfache Datentypen aus den _Built-in-Datatypes_ auswählen,

- ... für die Datentypen (unterschiedliche) Literale nennen, die gültige Werte beschreiben,

- ... eigene Datentypen über _Restrinctions_, _Unions_  und erstellen.

</span>

#### Datentypen 

Im Gegensatz zu DTD können in XML Datentypen genauer spezifiziert - sogar im Inhalt deutlicher eingegrenzt werden. Hierbei bieten Schema derart viele Möglichkeiten, dass dies sogar eine eigene unabhängige Spezifikation wurder mit vielen [mehrseitiges Kapiteln](https://www.w3.org/TR/xmlschema11-2).

Wir haben bislang nur einfache Zeichenketten genutzt (`type="xsd:string"`), die komplexen Elemente (mit Unterelementen und/oder Attributen) oder den irgendwas-"Joker" (`type="xsd:anyType"`). Der Sprachumfang differenziert jedoch sehr umfangreich weiter aus, wie die folgende Übersicht der Built-in-Datentypen zeigt (die man ähnlich [in der Spezifikation findet](https://www.w3.org/TR/xmlschema11-2/#built-in-datatypes)):

![Eine Liste der XML-Datentypen als Pseudo-UML-Vererbungsstruktur](plantuml/xsd-datentypen.png)

Bei der Definition von Datentypen werden unterschiedliche Eigenschaften gegeneinander abgegrenzt. Beist die Zähne zusammen, durch diese Theorie müssen wir kurz durch:

- Der **Werteraum (_value space_)** beschreibt den Satz (_set_) der Werte eines Datentyps. Bei `byte` sind das z.B. alle Ganzzahlen zwischen `0` und `255`.

- Der **lexikalische Raum (_lexical space_)** beschreibt ein Set derjenigen Zeichenketten, die auf Werte des Werteraums gemappt werden können (_lexical mapping_). Bei `byte` wären das alle reinen Ziffernfolgen von `-128` bis `127`, aber eben auch `+17`, `00124`, `-010` - also andere Zeichenketten, die eindeutig als `byte` ausgewertet werden können. Welche das jeweils sind definiert der Datentyp.

- **Literale** sind Zeichenketten aus (keinem oder mehr) Zeichen des Universal Character Set (UCS - der Zeichensatz, der die Basis für UTF ist). Literale können gültige Repräsemtationen eines spezifischen Datentyp-Werts sein (dann wären sie im lexikalischen Raum). Sie können aber auch ungültige Zeichenketten sein, für die es kein lexikalisches Mapping gibt. 

Wem das zu abstrakt klingt: Der Wert `1`, der sich im Werteraum einer Ganzzahl (z.B. `int`) befindet, kann durch verschiedene lexikalische Repräsentationen (_lexical representations_) ausgedrückt werden: `+1`, `01`, `0001` usw. Sie alle stellen Literale dar, die auf den Wert `1`  gemappt werden. Es gibt aber auch Literale, die auf keinen Wert im Datentyp `int` gemappt werden: `1.0` beispielsweise oder `eins`.

Bei einer Festkommazahl (`decimal`) stellen beispielsweise die Literale `123.45`, `0123.45`, `123.450` oder `+123.45` lexikalische Repräsentationen dar. 

Ein besonderes Literal für jeden Wert stellt hierbei die **kanonische Repräsentation (_canonical representation_)** dar. Diese Repräsentation wird so definiert, dass es für jeden möglichen Wert des Werteraums ein einzigartiges kanonisches Literal gibt. Es ist also eine Teilmenge des lexikalischen Raums, die eine eins-zu-eins-Beziehung zum Werteraum aufspannt (_canonical mapping_). Für eine Festkommazahl ist beispielsweise festgelegt, dass die kanonische Repräsentanz keine führende/endende Null und bei positiven Zahlen kein Vorzeichen enthält.

Der Werteraum ist zunächst für alle vorgegebenen Typen definiert, kann aber über **Facetten** für eigene Typen weiter eingegrenzt werden.  

Die wesentlichen Datentypen in der Kurzübersicht:

|Datentyp|Beispiel|Eigenschaften|
|:---|:---|:---|
|`string`|Jeder beliebige Text|
|`dateTime`|`1999-05-31T13:20:00.000-05:`|Für die einzelnen Teilbereiche gibt es eigene Datentypen (`date`, `time`, `gMonth`...)|
|`long`|von `-9223372036854775808`, <br/> über ... `-1`, `0`, `1`, ...<br/> bis `9223372036854775807`|64 Bit, vorzeichenbehaftet, basiert auf `integer`|
|`int`|	von `-2147483648`, <br/>  über ... `-1`, `0`, `1`, <br/> bis ... `2147483647`|32 Bit, vorzeichenbehaftet, basiert auf `long`|
|`float`|`-INF`, `INF`, `NaN` <br/>`-1E4`, `-0`, `0`, `12.78E-2`, `12`|32-bit Gleitkommazahl: geringere Präzision, geringere Bereichsgrenze als `double`|
|`double`|`-INF`, `INF`, `NaN` <br/> `-1E304`, `-0`, `0`, `12.78E-2`, `12`|64-bit Gleitkommazahl: größere Präzision, größerer Definitionsbereich als `float`|
|`boolean`|`true`<br/>`false`<br/>`0`, `1`|	
|`base64Binary`|`Zh8+H6zd`|Gültige Zeichen sind `[A–Za–z0–9+/]`|	 

#### Ableitung eigener Datentypen durch Einschränkung (Restriction)

Über _Restrictions_ können auf Basis der `simpleTypes` eigene Datentypen erstellt werden. Jeder Datentyp bringt dafür einen eigenen Kanon an möglichen _Constraining facets_ mit. Neue Typen werden über ein `<xs:simpleType />`-Element deklariert. Die _Constraining facets_ werden als _Restrictions_ bezogen auf einen Basis-Typen (_base_) angegeben (`<xs:restriction base="xs:string">`). Insgesamt sieht das beispielsweise so aus:

```xml
  <xs:simpleType name="e-mail">
    <xs:restriction base="xs:string">
      <xs:pattern value="[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+" />
    </xs:restriction>
  </xs:simpleType>
```

Der volle Umfang der _Constraining facets_ lässt sich in der Spezifikation nachlesen, die wesentlichen sind etwa folgende:

|_Constraining facets_|Beschreibung<br/>Constraint gibt es bei...|Beispiel|
|:---|:---|:---|
|`pattern`|Value muss RegEx erfüllen<br/>möglich an allen Datentypen bis auf `boolean`|`<xs:restriction base="xs:string">`<br/>&nbsp;&nbsp;&nbsp;&nbsp;`<xs:pattern value="|0-9A-F]+" />`<br/></xs:restriction>|
|`enumeration`|Wert muss einem der gelisteten Werte entsprechen<br/>möglich an allen Datentypen bis auf `boolean`|`<xsd:restriction base="xsd:string">`<br/>&nbsp;&nbsp;&nbsp;&nbsp;`<xsd:enumeration value="ja"/>`<br/>&nbsp;&nbsp;&nbsp;&nbsp;`<xsd:enumeration value="nein"/>`<br/>`</xsd:restriction>`|
|`whiteSpace`|Wie wird mit Whitespaces umgegangen (carriagereturn, linefeed, tabs, spaces)?<br/> `preserve` = keine Normalisierung,<br/>`replace`= alle werden zu Spaces, <br/>`collapse`= wie `replace` + doppelte Spaces und Spaces an Beginn/Ende werden entfernt<br/>möglich an  `string` sowie `hexBinary`, `base64Binary`|`<restriction base='normalizedString'>`<br/>&nbsp;&nbsp;&nbsp;&nbsp;`<whiteSpace value='collapse'/>`<br/>`</restriction>`|
|`length`<br/>`minLength`<br/>`maxLength`|Legt die (Mindest-/Maximal-)Anzahl der Zeichen fest,die der Wert enthalten darf.<br/>möglich an allen `string`|`<restriction base='string'>`<br/>&nbsp;&nbsp;&nbsp;&nbsp;`<minLength value='1'/>`<br/>`</restriction>`|
|`minExclusive`<br/>`minInclusive`<br/>`maxExclusive`<br/>`maxInclusive`|Legt die untere/obere Grenze des Zahlenbereichs fest (inkl. bzw. exkl. dieser Grenze)<br/>möglich an allen Zahlentypen|`<restriction base='integer'>`<br/>&nbsp;&nbsp;&nbsp;&nbsp;`<maxInclusive value='100'/>`<br/>`</restriction>`|
|`fractionDigits`<br/>`totalDigits`|Legt die Anzahl der Nachkomma-Stellen bzw. der Stellen insgesamt fest (also inkl. Nachkommastellen)<br/>möglich an `decimal`|`<restriction base='decimal'>`<br/>&nbsp;&nbsp;&nbsp;&nbsp;`<fractionDigits value='1'/>`<br/>&nbsp;&nbsp;&nbsp;&nbsp;`<totalDigits value='4'/>`<br/>`</restriction>`
|


#### Zeichenketten-Datentypen

|Datentyp|Beispiel||
|:---|:---|:---|
|`string`|Jeder beliebige Text|
|`normalizedString`|Basiert auf `string`, aus denen _Carriage Return_, _Line Feed_ und Tabs entfernt wurden|
|`token`|Basiert auf `normalizedString`, die keine _Spaces_ am Anfang und Ende und keine doppelten _Spaces_ hintereinander enthalten.|
|`anyURI`|http://www.oer-informatik.de/xml-schema|  	

#### Datums / Zeittypen

|Datentyp|Beispiel||
|:---|:---|:---|
|`duration`|P1Y2M3DT10H30M12.3S|
|`dateTime`|1999-05-31T13:20:00.000-05:|
|`time`|13:20:00.000|
|`date`|1999-05-31|
|`gYearMonth`|1999-02|
|`gMonthDay`|--05-31|
|`gYear`|1999|
|`gMonth`|--05|
|`gDay`|---31|

#### Ganzahlen- und Festkommazahltypen

|Datentyp|zulässige Literale||
|:---|:---|:---|
|`decimal`|-1.23<br/>0<br/>123<br/>+001000.00|führendes Plus und führende/endende Nullen dürfen weggelassen werden.|
|`integer`|-1, 0, 1|Basiert auf `decimal`.
|`nonNegativeInteger`|0, 1, 2|Basiert auf `integer`|
|`positiveInteger`|1, 2,|Basiert auf `nonNegativeInteger`|
|`nonPositiveInteger`| -2, -1, 0|Basiert auf `integer`|
|`negativeInteger`|-2, -1|Basiert auf `nonPositiveInteger`|
|`long`|-9223372036854775808, ... -1, 0, 1, ... 9223372036854775807|64 Bit, vorzeichenbehaftet, basiert auf `integer`|
|`unsignedLong`|	0, 1, ... 18446744073709551615|64 Bit,vorzeichenlos, basiert auf `nonNegativeInteger`|
|`int`|	-2147483648, ... -1, 0, 1, ... 2147483647|32 Bit, vorzeichenbehaftet, basiert auf `long`|
|`unsignedInt`|	0, 1, ...4294967295|32 Bit vorzeichenlos, basiert auf `unsignedLong`|
|`short`|	-32768, ... -1, 0, 1, ... 32767|16 Bit, vorzeichenbehaftet, , basiert auf `int`|
|`unsignedShort`|	0, 1, ... 65535|16 Bit vorzeichenlos, basiert auf `unsignedInt`|
|`byte`|	-128, ...-1, 0, 1, ... 127|8 Bit, vorzeichenbehaftet, , basiert auf `short`|
|`unsignedByte`|	0, 1, ... 255|8 Bit vorzeichenlos, basiert auf `unsignedShort`|

#### Gleitkommatypen

|Datentyp|Beispiel||
|:---|:---|:---|
|`float`|-INF, -1E4, -0, 0, 12.78E-2, 12, INF, NaN|32-bit|
|`double`|-INF, -1E4, -0, 0, 12.78E-2, 12, INF, NaN|64-bit|

#### Weitere Datentypen

|Datentyp|Beispiel||
|:---|:---|:---|
|`boolean`|true<br/>false<br/>0, 1|	
|`base64Binary`|GpM7	 
|`hexBinary`|0FB7	 
	
#### XML-Spezifische Datentypen

|Datentyp|Beispiel||
|:---|:---|:---|
|`Name`|adresse|Basiert auf `token`. Für XML-Elemente zulässiger String (z.B. keine Zahlen am Anfang).
|`QName`|	kunde:adresse|	voller Name eines Elements/Attributs (Bsp.: `namespaceprefix:localpart`)
|`NCName`|adresse|Basiert auf `Name`._local part_  eines Element-/Attributnamens ("non-colonized").
|`language`|`de-DE`<br/>`en-US`|Basiert auf `token`. Zeichenketten, die gemäß XML 1.0 Standard eine Sprache repräsentieren|
|`ID`|		|Ein einzigartiger `Name`, der ein Element identifiziert|
|`IDREF`|		|Muss dem Wert einer `ID` entsprechen, da auf diese referenziert wird.|
|`IDREFS`|		|Space-getrennte Liste von `IDREF`|
|`ENTITY`|		|XML 1.0 ENTITY attribute type
|`ENTITIES`|		|Space-getrennte Liste von `ENTITY`|
|`NOTATION`|		|Set aus allen `QNames` des Namespaces des aktuellen XML-Schema|
|`NMTOKEN`|	US,Brésil	|Basiert auf `token`. NMToken nutzen die gleichen Zeichen wie `Name`, haben aber nicht die Einschränkung mit bestimmten Zeichen nicht beginnen zu dürfen.|
|`NMTOKENS`|US UK,Brésil Canada Mexique|Space-getrennte Liste von `NMTOKEN`|




#### Links und weitere Informationen

- W3C-Dokumente zu XML Schema: [Primer (Kurzübersicht)](https://www.w3.org/TR/xmlschema-0/)/ [Part 1: Structures](https://www.w3.org/TR/xmlschema-1/) / [Part 2: Datatypes](https://www.w3.org/TR/xmlschema-2/)

- [Der Standard zu Namespaces in XML](https://www.w3.org/TR/xml-names/)

- [Der XML-Standard 1.0](https://www.w3.org/TR/xml/)

- [Der seltener genutzte XML-Standard 1.1](https://www.w3.org/TR/xml11/)

- Infos von [w3schools](https://www.w3schools.com/xml/default.asp) zu XML
