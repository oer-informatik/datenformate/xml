## Die Auszeichnungssprache Extensible Markup Language (XML)

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111959976268780082</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/xml-einstieg</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Die Extensible Markup Language (XML) wird häufig zur Speicherung hierarchischer Daten genutzt. Der allgemeine Aufbau von XML und das Hauptmerkmal - die Wohlgeformtheit - wird im Artikel beschrieben. Dieser Artikel ist der erste Teil einer Artikelserie zu XML._

Dieser Artikel ist Bestandteil einer Serie zu den _XML_-Grundlagen:

- Teil 1: [XML-Grundlagen und Wohlgeformtheit: Hierarchische Daten mit XML speichern](https://oer-informatik.de/xml-einstieg) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 2: [XML-Dokumente gegen eine Document-Type-Definition validieren](https://oer-informatik.de/xml-dtd) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 3: [Namespaces für komplexere XML-Dokumente definieren](https://oer-informatik.de/xml-namespaces) 

- Teil 4: [Die Struktur von XML-Dokumente gegen ein XSD-Schema validieren](https://oer-informatik.de/xml-schema-struktur)

- Teil 5: [Datentypen-Definitionen eines XML-Schema zur Validierung nutzen](https://oer-informatik.de/xml-schema-datentypen)

<span class="kann-liste">

Nach Durchlesen des Artikels kann ich...

- ... Anwendungsfälle und Alleinstellungsmerkmale des Datentyps XML benennen,

- ... die Kriterien nennen und anwenden, die eine XML-Datei erfüllen muss, um wohlgeformt zu sein,

- ... den Unterschied zwischen einer wohlgeformten und validen XML-Datei erklären,

- ... verschiedene Möglichkeiten benennen, mithilfe derer ich Zeichen in XML maskieren kann,

- ... und den Unterschied und die Gemeinsamkeiten zwischen HTML und XML benennen.

</span>

### Wofür wird XML verwendet? Anwendungsfälle und Beispiele

Die Auszeichnungssprache XML dient zur Speicherung hierarchischer Daten, deren Struktur (und ggf. auch Datenformat) validierbar sein und deklarativ beschrieben werden sollen. 

Die XML erinnert durch ihren Aufbau mit _Tags_ (von spitzen Klammern umschlossene Bereiche) stark an die Internet-Auszeichnungssprache HTML. Tatsächlich stammen beide von der [_Standard Generalized Markup Language (SGML)_](https://de.wikipedia.org/wiki/Standard_Generalized_Markup_Language) ab, die aber selbst heute keine große Rolle mehr spielt und weitgehend unbekannt ist. 

![Sowohl XML als auch HTML erben von der SGML. XHTML ist wiederum eine Implementierung der XML mit den Elementen von HTML](images/SGML.png)


XML-Dokumente zeichnen sich dadurch aus, dass

- Daten hierarchisch in einer Baumstruktur abgelegt werden,

- die grundlegende Syntax immer gewährleistet ist, wenn das XML-Dokument den Regeln der Wohlgeformtheit entspricht,

- die Struktur der Daten festlegbar und überprüfbar (validierbar) ist (mit DocumentTypeDefinitions oder Schema),

- der Inhalt der Daten festlegbar und überprüfbar (validierbar) ist  (mit Schema),

- das Format beliebig erweiterbar ist und

- es sich um einen weitverbreiteten Standard des W3C handelt.

#### Beispiele für XML-Dateien

XML-Dateien begegnen uns in der Praxis an sehr unterschiedlichen Stellen:

- Die gängigen Office-Dokumente sind gezippte XML-Dateien: Das gilt sowohl für das OpenXML-Format von Microsoft (`*.docx`/`*.xlsx`) als auch für das OpenDocumentFormat, wie es z.B. LibreOffice nutzt (`*.odf`). (Wer es nicht glaubt: einfach mal ein Word-Dokument entzippen und die entpackten Dateien im Editor öffnen).

- Die Beschreibung von grafischen Benutzeroberflächen (GUI) wird häufig als XML-Struktur abgelegt (_XAML_ bei `C#`/WPF, [_FXML_ bei JavaFX](https://openjfx.io/javadoc/21/javafx.fxml/javafx/fxml/doc-files/introduction_to_fxml.html), _XHTML_  als XML-Variante für Websites)

- Als RSS-Feeds, mit denen wir Nachrichten, Podcasts usw. folgen können sind als XML aufgebaut (z.B. der Feed von dieser Seite ist [hier erreichbar](view-source:https://oer-informatik.de/feed)).

- XML wird genutzt zur Speicherung der Daten von Outdoor-Apps, als Konfigurationsdatei (Beispielsweise die [pom-xml](https://gitlab.com/oer-informatik/qs/code-coverage/-/raw/main/jacoco-metrik-example/pom.xml?ref_type=heads) des Build-Tools Maven). 

#### Warum wird nicht einfach JSON, YAML oder CSV genutzt? Was ist das Alleinstellungsmerkmal von XML?

Ohne einen tieferen Blick in die Bestandteile von XML geworfen zu haben, fallen zunächst zwei Unterschiede auf:

- Im Vergleich zu CSV-Dateien: XML-Dokumente sind gut geeignet, um ein hierarchisches Datenmodell (eine Baumstruktur) abzubilden. CSV-Dateien eignen sich für relationale Modelle (also nicht für Baumstrukturen, sondern für Daten, die sich tabellarisch erfassen lassen).

- JSON und YAML sind zwar geeignet, um hierarchische Daten in variablen Baumstrukturen abzubilden, bieten aber keine Möglichkeit, die Datenstruktur und den Dateninhalt innerhalb des Datenformats zu beschreiben und über einen allgemeinen Parser zu validieren. Sowohl die Dokumentation als auch die Validierung jeweils individuell programmiert werden.

XML-Dokumente verfügen über zwei Qualitätsmerkmale:

- Sie _müssen_ wohlgeformt (_well formed_) sein. Wohlgeformt ist die Bezeichnung dafür, dass ein Dokument den allgemeinen XML-Regeln entspricht. Nur wohlgeformte XML-Dateien werden von Parsern interpretiert. Die Regeln folgen in den kommenden Absätzen.

- Darüberhinaus _können_ wohlgeformte XML-Dateien in Struktur und Inhalt einem definierten Aufbau entsprechen: Man spricht dann von gültigen (_valid_) XML-Dokumenten. Valide können XML-Dateien gegen ein XML-Schema (XSD) oder gegen eine DocumentTypeDefinition (DTD) sein. Die Erklärung dieser beiden Eigenschaften folgt in weiteren Artikeln.

### Bestandteile eines XML-Dokuments

Wir betrachten als Beispiel eine vereinfachte GPX-Datei einer Outdoor-App:

![Beispiel einer GPX-XML-Datei, in der Prolog, Wurzelelement, Start- und Endtags sowie ein leeres Element gekennzeichnet sind.](images/xml-uebersicht.png)

#### Kernbestandteil von XML-Dokumenten sind Elemente:
   
   - Elemente werden in _Tags_ eingefasst. Es gibt öffnenden Tags (_Starttag_: `<name>`), deren Namen nach der spitzen Klammer steht, und schließende Tags (_Endtag_: `</name>`), bei denen ein _Slash_ dem _case-sensitive_-Tagnamen des zugehorigen Starttags vorangestellt wird. Zwischen diesen beiden Tags steht der Elementinhalt (_Content_).

   - Elemente ohne Inhalt können in Kurzschreibweise mithilfe des _empty Element Tags_ abgekürzt werden: hier wird _Starttag_ und _Endtag_ in einem einzigen Tag zusammengefasst, der mit dem _Slash_ vor der spitzen schließenden Klammer beendet wird: (statt `<link href="https://www.komoot.de"><link/>` kann man kurz schreiben: `<link href="https://www.komoot.de" />`)

  - Das erste sich öffnende Element nennt sich Wurzelelement (_Root-Element_ oder _Document Element_). Im obigen Beispiel ist das Wurzelelement `<gpx ...>...</gpx>`. Es darf auf der Ebene des _Root Elements_ kein zweites Element geben.

   - Neben Elementeinhalt (_content_, also Text oder weitere Elemente zwischen öffnenden und schließendem Tag) können Elemente auch Daten in Attributen speichern. Diese werden als _Key_/_Value_-Paare durch Gleichheitszeichen getrennt, wobei die _Values_ in einfache oder doppelte Anführungszeichen eingeschlossen werden. Innerhalb eines Tags darf jeder _Key_ nur einmal vorkommen und die _Values_ dürfen keine schließende spitze Klammer enthalten. Im obigen Beispiel finden sich Attribute in dem Element `<trkpt lat="52.022650" lon="8.549855" />`.´

#### Allgemeiner Aufbau rund um das Wurzelelement

Ein XML-Dokument besteht aus dem Prolog und dem Wurzelelement. 

Der Prolog ist optional. Er beginnt mit der ebenso optionalen XML-Deklaration - im obigen Beispiel `<?xml version='1.0' encoding='UTF-8' standalone='yes'?>`. Falls eine XML-Deklaration enthalten ist, muss diese direkt am Anfang stehen. 

Weiterhin können im Prolog Verarbeitungshinweise (_Processing Instructions_) stehen sowie - wie überall in XML-Dokumenten - _Whitespaces_ und Kommentare.

Kommentare werden zwischen `<!--` und `-->` eingeschlossen (wobei das jeweils erste und letzte Zeichen des Kommentarinhalts kein weiteres `-` sein darf): `<!-- gültiger Kommentar -->` und `<!---ungültiger Kommentar--->`. Innerhalb eines Kommentars darf kein doppelter Bindestrich vorkommen: `<!--ungültiger--Kommentar-->`

Verarbeitungshinweise (_Processing Instructions_) werden zwischen `<?` und `?>` eingeschlossen. Mit ihnen werden beispielsweise _DocumentType-Definitions_ eingebunden, die Vorgaben an die Struktur des XML-Dokuments definieren.

Auf den Prolog folgt das eine Wurzelelement und dessen Elementinhalt. Innerhalb des Wurzelelements dürfen auch Kommentare und Whitespaces genutzt werden.

Optional dürfen nach dem Wurzelelement nur noch Kommentare, Verarbeitungshinweise oder Whitespaces folgen.


### Die Regeln der Wohlgeformtheit (_well-formed constraints_)

Ein XML-Dokument ist [wohlgeformt (_well-formed_)](https://www.w3.org/TR/xml/#sec-well-formed), wenn (kursiv in Klammer die Bezeichnungen im Standard):

- End-Tags denselben _case-sesitiven_ Namen tragen wie zugehörige Start-Tags 

- es genau ein Root-Element (auch _document element_ genannt) gibt,

- vor dem Root-Element optional der Prolog, nach dem Root-Element höchstens Kommentare oder Verarbeitungshinweise stehen,

- alle nicht leeren Elemente mit einem Endtag gleichen Namens (case-sensitiv) geschlossen werden (_Element Type Match_)

- alle Elemente innerhalb derselben Elemente geschlossen werden, innerhalb derer sie geöffnet werden (also in gleicher Reihenfolge schließen wie öffnen),

- enthaltene Texte sich aus Unicode-Zeichen zusammensetzen (bis auf wenige nicht erlaubte Steuerzeichen, siehe [link](https://www.w3.org/TR/xml/#charsets)) (_Legal Character_),

- enthaltene Element- und Attributnamen nicht mit Punkt, Zahlen, der Zeichenfolge "xml|XML|xMl..." und Bindestrich beginnen (sowie weiteren, weniger gebräuchlichen Zeichen wie dem Malpunkt `#xB7` und diakritischen Zeichen `#x0300-#x036F` und `#x203F-#x2040`) 

- Attributnamen innerhalb eines Tags nur einmal vorkommen (_Unique Att Spec_),

- Attributwerte in einfachen oder doppelten Anführungszeichen eingeschlossen sind und kein `<` enthalten (_No `<`` in Attribute Values_),

- Attributwerte keine externen Entitäten referenzieren (_No External Entity References_)

- Document-Type-Definition (falls vorhanden) sich auch an die Regeln der Wohlgeformtheit halten (_PEs in Internal Subset_, _External Subset_, _PE Between Declarations_)

- alle referenzierten Entitäten auch wohlgeformt sind (hierzu im nächsten Artikel mehr, _Entity Declared_, _Parsed Entity_, _No Recursion_, _In DTD_)


Alle Parser brechen die Verarbeitung von nicht wohlgeformten XML-Dateien ab. Auch Browser haben in der Regel einen integrierten XML-Parser, mit denen man ein [wohlgeformtes XML-Dokument](xml-beispiele/xml-beispiel-gpx.xml) betrachten und bei einem [nicht wohlgeformten XML-Dokument](xml-beispiele/xml-beispiel-gpx.xml) eine Fehlermeldung erhält.

#### Wo können in XML überall Nutzdaten gespeichert werden?

In XML-Dokumenten können Daten auf unterschiedliche Weise an verschiedenen Stellen gespeichert werden:

- als Elementeinhalt zwischen Start- und Endtag,

- als Attributwert zwischen den einfachen oder doppelten Anführungszeichen,

- als Elementname,

- als Attributname und

- in der Struktur (Reihenfolge) der Elemente.

(Zusätzlich können Informationen natürlich auch in Kommentaren stehen, wenngleich das ein denkbar schlechter Ort ist.)


#### Ungültige Zeichen, CDATA und Entities

Die Daten können nahezu den ganzen Unicode-Zeichenumfang nutzen (bis auf wenige nicht erlaubte Steuerzeichen, siehe [link](https://www.w3.org/TR/xml/#charsets)) und als normale Zeichenketten gespeichert werden. Jedoch werden beispielsweise die spitzen Klammern als XML-Bestandteil interpretiert. Es ist daher nicht einfach so möglich, XML- oder HTML-Tags als Content in XML-Dateien zu speichern. 

Ein Weg, Daten zu maskieren, damit sie nicht als XML-Markup interpretiert werden, stellen die **CDATA**-Blöcke dar, die mit `<![CDATA[` eingeleitet und mit `]]>` abgeschlossen werden. In ihnen darf alles enthalten sein, außer die Zeichenfolge `]]>`. Wenn also in XML beispielsweise ein fettgeschriebener HTML-Textschnipsel gespeichert werden soll, so lautet das zugehörige CDATA-Tag: 

```xml
<![CDATA[ <b>bold</b> ]]>
```

XML bietet einige interne **Entities**, die genutzt werden können, Konflikte mit Zeichen besonderer Bedeutung zu verhindern:

- `&lt;` repräsentiert das `<` (relevant bei mathematischen Vergleichen oder wenn Tags als Inhalt gespeichert werden sollen, z.B. `&lt;b&gt;bold&lt;/b&gt;`)

- `&gt;` repräsentiert das `>` (relevant bei mathematischen Vergleichen oder  wenn Tags als Inhalt gespeichert werden sollen, z.B. `&lt;b&gt;bold&lt;/b&gt;`)

- `&amp;` repräsentiert das `&` (relevant in allen Zeichenketten, z.B. `<text>Hinz&Kunz</text>`)

- `&apos;` repräsentiert das `'` (relevant in einfach angeführten Attributwerten, z.B: `stringdelimiter='&apos;'`)

- `&quot;` repräsentiert das `"` (relevant in doppelt angeführten Attributwerten, z.B: `stringdelimiter="&quot;"`)

Auf die Möglichkeit, eigene Entities zu definieren, gehe ich im DTD-Artikel ein.

#### Zum Abschluss: Welcher Standard ist derzeit gültig?

Die derzeit aktuelle und meist genutzte Version des Standards heißt nach wie vor Version 1.0. Sie ist mittlerweile in der "Fifth Edition" verfügbar, die sich nur unwesentlich (v.a. im Umgang mit Unicode) von den vorigen Editionen unterscheidet. Sie findet sich [hier (XML  1.0, Fifth Edition)](https://www.w3.org/TR/xml/). 

Es existiert parallel eine weniger verbreitete Version, irritiernderweise mit höherer Versionsnummer: [XML 1.1 (link)](https://www.w3.org/TR/xml11/). In dieser wurden neue Wege beschritten, Unicode und Zeilenumbrüche einzubinden, die später auf anderem Weg in die oben genannte Fifth Edition eingeflossen sind. Für die allermeisten Anwendungsfälle ist XML 1.0 der Weg der Wahl. Wer mit besonderen Zeilenumbrüchen operieren muss (`#x85`) oder darauf angewiesen ist, Element- und Attributnamen aus einem größeren Umfang von Unicode zu erstellen, als dies bei XML 1.0 möglich ist, der möge einen Blick auf die Unterschiede werfen, die [hier](https://www.w3.org/TR/xml11/#sec-xml11) konkreter beschrieben werden

### Links und weitere Informationen

- [Der XML-Standard 1.0](https://www.w3.org/TR/xml/)

- [Der seltener genutzte XML-Standard 1.1](https://www.w3.org/TR/xml11/)

- Infos von [w3schools](https://www.w3schools.com/xml/default.asp) zu XML
