## Die Struktur von XML-Dokumente gegen ein Schema validieren

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111970765687640258</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/xml-schema-struktur</span>

> **tl/dr;** _(ca. 10 min Lesezeit): Wenn XML-Dokumente spezifischen Regeln an die Abfolge von Elementen und Attributen genügen nennt man sie valide (gültig). Neben den DTD (aus dem zweiten Teil der Artikelserie) stellen XML Schema (XSD) eine Definition für gültige XML-Dokumente zur Verfügung. Der Aufbau/Struktur und die grundsätzlichen Eigenschaften einer Schema-Datei werden hier beschrieben._


Dieser Artikel ist Bestandteil einer Serie zu den _XML_-Grundlagen:

- Teil 1: [XML-Grundlagen und Wohlgeformtheit: Hierarchische Daten mit XML speichern](https://oer-informatik.de/xml-einstieg) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 2: [XML-Dokumente gegen eine Document-Type-Definition validieren](https://oer-informatik.de/xml-dtd) mit zugehörigen [Übungsaufgaben](https://oer-informatik.de/xml-einstieg-uebung)

- Teil 3: [Namespaces für komplexere XML-Dokumente definieren](https://oer-informatik.de/xml-namespaces) 

- Teil 4: [Die Struktur von XML-Dokumente gegen ein XSD-Schema validieren](https://oer-informatik.de/xml-schema-struktur)

- Teil 5: [Datentypen-Definitionen eines XML-Schema zur Validierung nutzen](https://oer-informatik.de/xml-schema-datentypen)

<span class="kann-liste">

Nach Durchlesen des Artikels kann ich mithilfe eines XML-Schema für gültige XML-Instanzen...

- ... Elemente definieren,

- ... Elemente als Sequenz oder Alternative anordnen,

- ... die Häufigkeit von Elementen festlegen,

- ... Elementen Attribute zuweisen.

</span>

Im XML-Standard ist die Validierung eines Dokuments gegen eine _Document-Type-Definition_ festgeschrieben. Diese hat jedoch eine Reihe von Nachteile:

- DTDs sind selbst nicht als XML formuliert, was häufig zu Problemen bei der Darstellung, der Bearbeitung und dem Verständnis der DTDs führt

- mit DTDs kann man zwar in Struktur der Elemente und Attribute definieren, jedoch nur sehr rudimentär deren Inhalte validieren.

Diese beiden Probleme adressiert die zweite Technik, mithilfe derer XML-Dokumente validiert werden können: die XML-Schema. 

Um XML-Schema verstehen zu können müssen die [Grundlagen von XML (wohlgeformtes XML)](https://oer-informatik.de/xml-einstieg) und die [Grundlagen von Namespaces](https://oer-informatik.de/xml-namespaces) bekannt sein. Sofern bei den Erläuterungen zu XML-Schema Unklarheiten entstehen, so ist die Spezifikation der W3C die Primärquelle, die zur Klärung herangezogen werden sollte. Veröffentlicht ist ein [Primer (Kurzübersicht)](https://www.w3.org/TR/xmlschema-0/) und eine zweiteilige Spezifikation: [Part 1: Structures](https://www.w3.org/TR/xmlschema-1/) / [Part 2: Datatypes](https://www.w3.org/TR/xmlschema-2/).

#### Einfacher Aufbau eines XML-Schemas

Um den Aufbau eines XML-Schemas kennenzulernen nutzen wir zunächst die gleiche XML-Instanz wie in der Beschreibung der DTDs:

```xml
<?xml version='1.0' encoding='UTF-8' ?>
<adressen>
    <adresse>
        <vorname>Max</vorname>
        <nachname>Mustermann</nachname>
        <telefon>
          <mobil>01234/567890</mobil>
        </telefon>
        <kommentar>
          Wohnt bei <vorname>Peter</vorname>
        </kommentar>
        <id personal_id="12345" mitarbeiter_id="6789"/>
    </adresse>
</adressen>
```

Dieser Aufbau wird beschrieben in einer _XML Schema Definition_ (XSD), die selbst ein _XML_-Dokument mit dem Rootelement `<schema />` ist, dessen Elemente dem Namensraum der URI "http://www.w3.org/2001/XMLSchema" angehören (in diesem Beispiel mit dem Präfix `xsd`, üblich ist auch `xs` zu nutzen). Es werden Elemente, deren Abfolgen und deren Typen definiert - sowie Attribute. In für das obige Beispiel sieht eine zugehörige Schema-Definition so aus:

```xml
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsd:element name="adressen">
        <xsd:complexType>
            <xsd:sequence>
                <xsd:element name="adresse"  minOccurs="1" maxOccurs="unbounded">
                    <xsd:complexType>
                        <xsd:sequence>
                            <xsd:element name="vorname">
                                <xsd:complexType mixed="true">
                                <xs:choice minOccurs="0" maxOccurs="unbounded">
                                    <xsd:attribute name="künstlername" type="xsd:string"/>
                                    <xsd:attribute name="ordensname" type="xsd:string"/>
                                </xs:choice>
                                </xsd:complexType>
                            </xsd:element>
                            <xsd:element name="nachname" type="xsd:string"/>
                            <xsd:element name="telefon" minOccurs="0" maxOccurs="1">
                                <xsd:complexType>
                                    <xsd:choice  minOccurs="0" maxOccurs="unbounded">
                                        <xsd:element name="mobil" type="xsd:string"/>
                                        <xsd:element name="festnetz" type="xsd:string"/>
                                    </xsd:choice>
                                </xsd:complexType>
                            </xsd:element>
                            <xsd:element name="kommentar" type="xsd:anyType"  minOccurs="0" maxOccurs="1"/>
                            <xsd:element name="id" minOccurs="0" maxOccurs="1">
                                <xsd:complexType>
                                    <xsd:attribute name="personal_id" type="xsd:string"/>
                                    <xsd:attribute name="mitarbeiter_id" type="xsd:string"/>
                                </xsd:complexType>
                            </xsd:element>
                        </xsd:sequence>
                    </xsd:complexType>
                </xsd:element>
            </xsd:sequence>
        </xsd:complexType>
    </xsd:element>
</xsd:schema>
```

#### Anordnung der Grundelemente einer Schema-Definition: `group`, `all`, `choice`, `sequence`

Einzelne, einfache Elemente werden jeweils mit Namen und einem vordefinierten Typ (hier: Zeichenkette) definiert:

```xml
<xsd:element name="vorname" type="xsd:string"/>
```

Sie werden mit dem `<element>` definiert - in meinem Beispiel im Namensraum `xsd`- aber das kann im `<schema />`-Element ja auch anders angegeben worden sein. Das gilt für alle folgenden Schema-Elemente.

Viele Elemente haben aber einen komplexeren Aufbau, weil sie Unterelemente (oder Attribute) enthalten (können). In diesem Fall muss angegeben werden, ob die Unterelemente als Sequenz (in fester Reihenfolge), als Alternative (nur eine der Möglichkeiten) oder beliebig folgen dürfen.

Bei der **Sequenz (`sequence`)** ist fest vorgegeben, in welcher Reihenfolge die Elemente angeordnet sind. Hierbei wird der Elementtyp nicht über das `type`-Attribut definiert, sondern über das Unterelement `<xsd:complexType>`, dem die Festlegung der Sequenz folgt: `<xsd:sequence>`:

```xml
<xsd:element name="adresse" >
    <xsd:complexType>
        <xsd:sequence>
            <xsd:element name="vorname" type="xsd:string"/>
            <xsd:element name="nachname" type="xsd:string"/>
            <xsd:element name="kommentar" type="xsd:anyType"/>
        </xsd:sequence>
    </xsd:complexType>
</xsd:element>
```

Bei der **Alternative (`choice`)** darf nur eines der genannten Unterelemente folgen. Der Aufbau ist analog zur Sequenz: im Unterelement `<xsd:complexType>` wird die Alternaitve mit dem `<xsd:choice>`-Element definiert. Hier darf also im `<telefon>` Elemente nur entweder ein `<mobil>`- oder ein `<festnetz>`-Element folgen.

```xml
<xsd:element name="telefon">
    <xsd:complexType>
        <xsd:choice>
            <xsd:element name="mobil" type="xsd:string"/>
            <xsd:element name="festnetz" type="xsd:string"/>
        </xsd:choice>
    </xsd:complexType>
</xsd:element>
```

Sofern die Unterelemente **in beliebiger Reihenfolge (`all`)** angeordnet sein dürfen, wird dies mit `<xsd:all>` definiert:


```xml
<xsd:element name="telefon">
    <xsd:complexType>
        <xsd:all>
            <xsd:element name="mobil" type="xsd:string"/>
            <xsd:element name="festnetz" type="xsd:string"/>
        </xsd:all>
    </xsd:complexType>
</xsd:element>
```

#### Häufigkeit der Unterelemente (_Occurence Constraints_)

Wenn keine Festlegung getroffen wird, wie häufig Unterelemente gelistet werden dürfen, dann müssen sie mindestens einmal (`minOccurs="1"`) und maximal einmal (`maxOccurs="1"`) genannt sein. Nur abweichende Werte hiervon müssen benannt werden. Im Gegensatz zur DTD können hier auch spezifische Werte angegeben werden.

|Anzahl|DTD|XSD|
|:---|---|:---|
|ein oder keinmal|`?`|`minOccurs="0" maxOccurs="1"`<br/>Da `maxOccurs` dem Defaultwert entspricht, kann dies weggelassen werden.|
|genau einmal|nichts|`minOccurs="1" maxOccurs="1"`<br/> Dar beides dem Defaultwert entspricht, kann es weggelassen werden.|
|keinmal oder häufiger|`*`|`minOccurs="0" maxOccurs="unbounded"`|
|einmal oder häufiger|`+`|`minOccurs="1" maxOccurs="unbounded"`<br/> Dar `maxOccurs` dem Defaultwert entspricht, kann dies weggelassen werden.|
|definierte Anzahl|nicht möglich|`minOccurs="2" maxOccurs="4"`|

Das Beispielschema enthält mindestens ein bis beliebig viele `<adresse />`-Elemente:

```xml
<xsd:element name="adresse"  minOccurs="1" maxOccurs="unbounded">
    <!-- ... -->
</xsd:element>
```

Das Element `<telefon />` ist optional:

```xml
<xsd:element name="telefon" minOccurs="0" maxOccurs="1">
    <!-- ... -->
</xsd:element>
```

Darin können keine oder viele Elemente von `<mobil />` oder `<festnetz />` enthalten sein:

```xml
<xsd:element name="telefon" minOccurs="0" maxOccurs="1">
    <xsd:complexType>
        <xsd:choice  minOccurs="0" maxOccurs="unbounded">
            <xsd:element name="mobil" type="xsd:string"/>
            <xsd:element name="festnetz" type="xsd:string"/>
        </xsd:choice>
    </xsd:complexType>
</xsd:element>
```

#### Attribute definieren

Attribute werden am Ende der `<complexType>` Definition angehängt (bei Elementen mit Unterelementen im Anschluss an die Elemente). Auch bei Attributen wird analog zu Elementen ein `name` und ein `type` angegeben. 

```xml
<xsd:element name="id">
    <xsd:complexType>
        <xsd:attribute name="personal_id" type="xsd:string"/>
        <xsd:attribute name="mitarbeiter_id" type="xsd:string"/>
    </xsd:complexType>
</xsd:element>
```

Ob Attribute zwingend erforderlich sind, kann über `use` angegeben werden:

|Notation|Bedeutung|Beispiel|
|:---|:---|:---|
|`use="required"`|Das Attribut **muss** angegeben werden|
|`use="optional"`|Das Attribut **kann** angegeben werden|
|`use="prohibited"`|Das Attribut **darf nicht** angegeben werden|

#### Elemente mit Inhalt und Unterelementen (gemischte Elemente / _mixed content_)

Es gibt Elemente, bei denen will man sowohl Unterelemente als auch Elementeinhalt in Form von Text. Hier kommt die _complexType_-Eigenschaft `mixed=true` ins Spiel:

```xml
<xsd:element name="vorname">
    <xsd:complexType mixed="true">
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xsd:attribute name="künstlername" type="xsd:string"/>
        <xsd:attribute name="ordensname" type="xsd:string"/>
      </xs:choice>
    </xsd:complexType>
</xsd:element>
```
Die Entsprechung in der DTD war: 

```xml
<!ELEMENT vorname (#PCDATA | künstlername | ordensname)*>
```

Wenn gar nicht festgelegt werden soll, welche Unterelemente und Inhalte es geben darf, dann kann dies mit dem Datentypen `anyType` geschehen:

```xml
<xsd:element name="kommentar" type="xsd:anyType"  minOccurs="0" maxOccurs="1"/>
```

#### leere Elemente

Auch bei leeren Elementen handelt es sich um komplexe Elemente. Sofern diese auch über keine Attribute verfügen, bleibt as `<complexType />` Element komplett leer:

```xml
<xsd:element name="id">
    <xsd:complexType />
</xsd:element>
```

#### Der Umgang mit `default` und `fixed`-Werten

Es können sowohl für Attribute als auch für Elemente Defaultwerte definiert werden, die vom Parser eingesetzt bzw. verarbeitet werden. Das Verhalten von Attributen und Elementen weicht hierbei leicht ab:

- Bei Attributen wird der Defaultwert `default="wert"` nur vom Parser bei Interpretation einer XML-Datei eingesetzt, wenn in der XML-Instanz das Attribut nicht angegeben war. Sobald es angegeben ist, wird der angegebene Wert beibehalten.

- Bei Elemente wird der Defaultwert nur dann vom Parser bei der Interpretation einer XML-Instanz eingesetzt, wenn in XML-Instanz das Element vorhanden, aber leer war. Wenn es nicht vorhanden war, wird das Element auch nicht ergänzt.

Eine andere Art der Defaultwerte wird über das `fixed`-Attribut festgelegt: hier darf der Wert nur den dort angegebenen Wert haben, andernfalls ist das XML-Dokument nicht gültig.

#### Globale Elemente und deren Referenz

Wir haben bislang Elemente immer dort deklariert, wo sie auch angeordnet waren. Das ist vor allem dann ein guter Weg, wenn sie nicht an unterschiedlichen Stellen im Dokument vorkommen.

Ein Schema bietet die Möglichkeit, Elemente _global_ zu deklarieren, in dem sie direkt unter dem `<schema>`-Element deklariert werden. 

```xml
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <xsd:element name="kommentar" type="xsd:anyType"  minOccurs="0" maxOccurs="1"/>
    <!-- ... -->
</xsd:schema>
```

Das so deklarierte Element kann jetzt an verschiedenen Stellen im XML-Baum referenziert werden:

```xml
<xsd:element ref="kommentar"/>
```


### Links und weitere Informationen

- W3C-Dokumente zu XML Schema: [Primer (Kurzübersicht)](https://www.w3.org/TR/xmlschema-0/)/ [Part 1: Structures](https://www.w3.org/TR/xmlschema-1/) / [Part 2: Datatypes](https://www.w3.org/TR/xmlschema-2/)

- [Der Standard zu Namespaces in XML](https://www.w3.org/TR/xml-names/)

- [Der XML-Standard 1.0](https://www.w3.org/TR/xml/)

- [Der seltener genutzte XML-Standard 1.1](https://www.w3.org/TR/xml11/)

- Infos von [w3schools](https://www.w3schools.com/xml/default.asp) zu XML
